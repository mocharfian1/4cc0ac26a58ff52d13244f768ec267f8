# Gunakan image PHP dari Docker Hub
FROM php:latest

# Set direktori kerja di dalam container
WORKDIR /var/www/html

# Salin file aplikasi PHP Anda ke dalam container
COPY . /var/www/html

# Expose port 80 untuk akses web
EXPOSE 8000

# Jalankan server PHP built-in pada port 80
CMD ["php", "-S", "0.0.0.0:8000"]
