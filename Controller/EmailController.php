<?php

require_once "Service/EmailService.php";

class EmailController {
    private EmailService $emailService;

    public function __construct() {
        $this->emailService = new EmailService();
    }

    public function sendEmail($request_data): string {
         $request = $this->emailService->sendEmail($request_data);
         return json_encode($request);
    }
}
