<?php

require_once "load_env.php";

class DatabaseConfig {
    private PDO $db;

    function __construct()
    {
        $dbHost = getenv("DB_HOST");
        $dbPort = getenv("DB_PORT");
        $dbName = getenv("DB_NAME");
        $dbUser = getenv("DB_USER");
        $dbPass = getenv("DB_PASS");

        $this->db = new PDO("pgsql:host=$dbHost;port=$dbPort;dbname=$dbName;user=$dbUser;password=$dbPass");
    }

    public function init_db(): PDO
    {
        return $this->db;
    }
}