<?php

require 'vendor/autoload.php';
require_once "load_env.php";

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class EmailConfig {
    private PHPMailer $mail;

    function __construct()
    {
        $mailHost = getenv("SMTP_HOST");
        $mailPort = getenv("SMTP_PORT");
        $mailName = getenv("SMTP_NAME");
        $mailUser = getenv("SMTP_USER");
        $mailPass = getenv("SMTP_PASS");

        $this->mail = new PHPMailer(true);

        $this->mail->isSMTP();
        $this->mail->Host       = $mailHost;
        $this->mail->SMTPAuth   = true;
        $this->mail->Username   = $mailUser;
        $this->mail->Password   = $mailPass;
        $this->mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
        $this->mail->Port       = $mailPort;

        try {
            $this->mail->setFrom($mailUser, $mailName);
        } catch (Exception $e) {

        }
    }

    public function init_email(): PHPMailer
    {
        return $this->mail;
    }
}