<?php


require_once "Config/DatabaseConfig.php";
require_once "Config/EmailConfig.php";

class EmailRepository {
    private PDO $db;
    private \PHPMailer\PHPMailer\PHPMailer $mail;

    function __construct() {
        $init = new DatabaseConfig();
        $this->db = $init->init_db();

        $initEmail = new EmailConfig();
        $this->mail = $initEmail->init_email();
    }

    public function sendEmail($emailTo = null, $subject = null, $body = null): array {
        try {
            $this->mail->addAddress($emailTo);
            $this->mail->Subject = $subject;
            $this->mail->Body    = $body;

            $sendEmail = $this->mail->send();

            if ($sendEmail) {
                return $this->logEmailToDB($emailTo, $subject, $body);
            } else {
                return array('success' => false, 'message' => 'Error send email');
            }
        } catch (Exception $e) {
            return array('success' => false, 'message' => 'Error insert DB');
        }

    }

    public function logEmailToDB($emailTo = null, $subject = null, $body = null): array {
        try {
            $storeDB = $this->db->prepare("INSERT INTO sent_emails (recipient, subject, body) VALUES (:emailTo, :subject, :body)");
            $storeDB->bindParam(':emailTo', $emailTo);
            $storeDB->bindParam(':subject', $subject);
            $storeDB->bindParam(':body', $body);
            $storeDB->execute();

            return array('success' => true, 'message' => 'Success send email & log into database.');
        } catch (Exception $e) {
            return array('success' => false, 'message' => 'Error insert DB');
        }

    }
}