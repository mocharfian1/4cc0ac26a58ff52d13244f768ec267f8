<?php



require_once 'Controller/EmailController.php';

class IndexApps{
    public function filterMethod(){
        $requestMethod = $_SERVER["REQUEST_METHOD"];

        switch ($requestMethod) {
            case 'POST':
                $this->POST_METHOD();
                break;
            case 'GET':
                $this->GET_METHOD();
                break;
            default:
                http_response_code(405);
                echo json_encode(array("message" => "Method Not Allowed."));
                break;
        }
    }

    public function POST_METHOD(){
        $path = isset($_SERVER['PATH_INFO']) ? $_SERVER['PATH_INFO'] : '/';
        $requestData = json_decode(file_get_contents("php://input"), true);

        $emailController = new EmailController();

        switch ($path) {
            case "/send_email":
                echo $emailController->sendEmail($requestData);
            break;
            default:
                http_response_code(405);
                echo json_encode(array("message" => "Path Not Allowed."));
                break;
        }
    }

    private function GET_METHOD()
    {
        $path = isset($_SERVER['PATH_INFO']) ? $_SERVER['PATH_INFO'] : '/';
        $requestData = json_decode(file_get_contents("php://input"), true);

        switch ($path) {
            case "/info":
                echo phpinfo();
                break;
            case "/login":
                echo $this->auth();
                break;
            case "/logout":
                echo $this->unauth();
                break;
            default:
                http_response_code(405);
                echo json_encode(array("message" => "Path Not Allowed."));
                break;
        }
    }

    private function auth(): bool{
        require_once 'vendor/autoload.php';

        session_start();

        $client = new Google_Client();
        $client->setAuthConfig('auth.json');
        $client->addScope(Google_Service_Drive::DRIVE_METADATA_READONLY); // Sesuaikan dengan ruang lingkup yang Anda butuhkan

        if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
            $client->setAccessToken($_SESSION['access_token']);
            return true;
        } else {
            $redirect_uri = 'http://' . $_SERVER['HTTP_HOST'] . '/oauth2callback.php';
            header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
            return false;
        }
    }

    private function unauth() {
        require_once 'vendor/autoload.php';
        unset($_SESSION['access_token']);

        $redirect_uri = 'http://' . $_SERVER['HTTP_HOST'] . '/login';
        header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
    }
}

$index = new IndexApps();
$index->filterMethod();

