<?php

require_once 'Repository/EmailRepository.php';
require_once 'Config/DatabaseConfig.php';

class EmailService {
    private EmailRepository $emailRepository;
    private DatabaseConfig $databaseConfig;

    public function __construct() {
        $this->emailRepository = new EmailRepository();
        $this->databaseConfig = new DatabaseConfig();
    }

    public function sendEmail($data): array {
        return $this->emailRepository->sendEmail($data['recipient'], $data['subject'], $data['body']);
    }
}
